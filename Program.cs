﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Stack<string> operatingSystems = new Stack<string>();
        
            operatingSystems.Push("Windows");
            operatingSystems.Push("Linux");
            operatingSystems.Push("macOS");
            operatingSystems.Push("Android");          
            Console.WriteLine("Елементи стеку у прямому порядку:");
            foreach (string os in operatingSystems)
            {
                Console.WriteLine(os);
            }            
            Console.WriteLine("\nЕлементи стеку у зворотньому порядку:");
            string[] osArray = operatingSystems.ToArray();
            for (int i = osArray.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(osArray[i]);
            }         
            Console.WriteLine("\nКількість елементів у стеку: " + operatingSystems.Count);
            operatingSystems.Clear();     
            Console.WriteLine("Кількість елементів у стеку після очищення: " + operatingSystems.Count);



            //Task 2

            Stack<int> stack = new Stack<int>();
            stack.Push(2);
            stack.Push(-5);
            stack.Push(10);
            stack.Push(7);
            stack.Push(-4);
            stack.Push(6);
          
            int sumOfPositive = 0;
            foreach (int number in stack)
            {
                
                if (number > 0 && number % 2 == 0)
                {

                    sumOfPositive += number;
                }
            }           
            if (sumOfPositive > 0)
            {
                Console.WriteLine("Сума додатних парних чисел у стеку: " + sumOfPositive);
            }
            else
            {
                Console.WriteLine("Додатних парних чисел у стеку немає");
            }
        }
    }
}
